#! /usr/bin/env python

from distutils.core import setup

setup(name='codeChecker',
      version='0.0',
      description='Dynamic Text File Checker',
      maintainer='Fabian Neuschmidt',
      maintainer_email='fabian@neuschmidt.de',
      url='www.test.de',
      scripts=['codeChecker'],
      packages=['filters', 'lib', 'src'],
      data_files=[('', ['codeChecker.conf']),
                  ('test', ['test/fine.c', 'test/test.c'])]
      )
