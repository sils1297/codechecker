#!/usr/bin/env python3

"""
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Lasse Schuirmann (lasse.schuirmann@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Lasse Schuirmann'

import Settings


class ColorPrinter:
    # private static
    __color_code_dict =\
        {
            'black': '0;30',
            'bright gray': '0;37',
            'blue': '0;34',
            'white': '1;37',
            'green': '0;32',
            'bright blue': '1;34',
            'cyan': '0;36',
            'bright green': '1;32',
            'red': '0;31',
            'bright cyan': '1;36',
            'purple': '0;35',
            'bright red': '1;31',
            'yellow': '0;33',
            'bright purple': '1;35',
            'dark gray': '1;30',
            'bright yellow': '1;33',
            'normal': '0',
        }

    __m_arg_dict = None

    def __init__(self):
        # private
        self.__m_pre = ""
        self.__m_post = ""

    def set_abstract_color(self, color):
        """
        Sets the color to values that will be retrieved from settings. This may
        be "WarningColor" or so.
        """
        # ensure that it is set yet
        assert isinstance(self.__class__.__m_arg_dict, dict)
        if not self.__class__.__m_arg_dict.__contains__(color):
            ColorPrinter.print_warning("WARNING: abstract color ({}) could not "
                                       "be retrieved.".format(color))
        self.set_color(self.__class__.__m_arg_dict.get(color, "normal"))

    def set_color(self, color):
        self.__m_pre = self.get_color_code(color)
        self.__m_post = self.get_color_code("normal")

    def print(self, *args, end="\n"):
        print(self.__m_pre, end='')
        for arg in args:
            print(arg, end=' ')
        print(self.__m_post, end=end)

    @staticmethod
    def set_arg_dict(arg_dict):
        if isinstance(arg_dict, Settings.Settings):
            ColorPrinter.__m_arg_dict = arg_dict.m_arg_dict
        else:
            assert isinstance(arg_dict, dict)
            ColorPrinter.__m_arg_dict = arg_dict

    @staticmethod
    def print_warning(*args, end="\n"):
        # ensure that it is set yet
        assert isinstance(ColorPrinter.__m_arg_dict, dict)
        col = ColorPrinter.__m_arg_dict.get("WarningColor", "yellow")
        ColorPrinter.print(col, "WARNING:", *args, end=end)

    @staticmethod
    def print_verbose(*args, end="\n"):
        # ensure that it is set yet
        assert isinstance(ColorPrinter.__m_arg_dict, dict)
        if ColorPrinter.__m_arg_dict.get('v', False):
            col = ColorPrinter.__m_arg_dict.get("VerboseInformationColor",
                                                "blue")
            ColorPrinter.print(col, "VERBOSE:", *args, end=end)

    @staticmethod
    def get_color_code(color):
        return '\033[' + ColorPrinter.__color_code_dict[color] + 'm'

    @staticmethod
    def print(color, *args, end="\n"):
        print(ColorPrinter.get_color_code(color), end='')
        for arg in args:
            print(arg, end=' ')
        print(ColorPrinter.get_color_code("normal"), end=end)

    @staticmethod
    def abstract_print(color, *args, end="\n"):
        # ensure that it is set yet
        assert isinstance(ColorPrinter.__m_arg_dict, dict)
        if not ColorPrinter.__m_arg_dict.__contains__(color):
            print("WARNING: abstract color ({}) could not be "
                  "retrieved.".format(color))
        ColorPrinter.print(ColorPrinter.__m_arg_dict.get(color, "normal"))
