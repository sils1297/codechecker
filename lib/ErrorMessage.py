#!/usr/bin/env python3

"""
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Lasse Schuirmann (lasse.schuirmann@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Lasse Schuirmann'


import ColorPrinter


class ErrorMessage:
    def __init__(self, line, message):
        #public
        self.m_line = line
        self.m_msg = message

    def print(self, end="\n"):
        ColorPrinter.ColorPrinter.abstract_print('FilterColor', 'Line ({}):'
                        .format(str(self.m_line)), end=' ')
        ColorPrinter.ColorPrinter.abstract_print('TextColor', self.m_msg,
                                                 end=end)
