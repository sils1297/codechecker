#!/usr/bin/env python3

"""
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Lasse Schuirmann (lasse.schuirmann@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Lasse Schuirmann'


import ColorPrinter


class Settings:
    def __init__(self, arg_dict):
        self.m_arg_dict = arg_dict

    def get_setting(self, setting_identifier, default):
        """
        Returns the value of the setting.

        :setting_identifier: a string that you find in the settings file
        :default: the default value
        """
        if self.m_arg_dict.get(setting_identifier, None) is None:
            ColorPrinter.ColorPrinter.print_warning(self.m_arg_dict,
                                       "{} is undefined. Setting default value "
                                       "({}).".format(setting_identifier,
                                                      default))
        val = self.m_arg_dict.get(setting_identifier, default)

        if str(val).lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup',
                                'certainly', 'uh-huh', 'j', 'ja']:
            val = True
        else:
            if str(val).lower() in ['false', '0', 'f', 'n', 'no', 'nope',
                                    'never']:
                val = False
            else:
                if str(val).isnumeric():
                    val = int(val)
        return val
