#!/usr/bin/env python3

"""
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Lasse Schuirmann (lasse.schuirmann@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Lasse Schuirmann'


import ErrorMessage


class FilterResult:
    def __init__(self, name):
        self.m_name = name
        self.m_err_list = []

    def append_error(self, err):
        assert isinstance(err, ErrorMessage.ErrorMessage)
        self.m_err_list.append(err)

    def append_error(self, line, message):
        self.m_err_list.append(ErrorMessage(self, line, message))
