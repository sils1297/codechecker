/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// This is a test document for the CodeChecker project

// TODO:	keyword match
// FIXME:	keyword match
// BUG:		keyword match

// More will follow...

int func(int param)
{
// a wrong indented comment
    // a right indented comment
    // TODO mixed tabs and spaces
    //  too  many  spaces
        // another wrong indented comment
    func(param); // a timeless loop. It's made for infinity, so buy it now!
    // this is not a too long line. I feel really guilty about writing this but
    // this is a far too long line. I feel really guilty about writing this but I have to
													  				//with tabs (and evil spaces)
}
