__author__ = 'fabian, lasse'

import os
import re
#
from src import colorPrinter


def list_target_file_paths(global_arg_dict):
    """ lists the absolute filepaths of all files that should be checked

    :returns: list of absolute filepaths

    """

    single_level_dirs = [os.path.abspath(filePath) for filePath in
                         global_arg_dict['d']]  # make them global
    multi_level_dirs = [os.path.abspath(filePath) for filePath in
                        global_arg_dict['dd']]  # make them global
    tested_file_types = global_arg_dict['t']
    ignored_file_types = global_arg_dict['i']

    file_paths = []

    # collect all files from single_level_dirs
    if single_level_dirs:
        for directory in single_level_dirs:
            colorPrinter.print_verbose(global_arg_dict, "NEXT SINGLELEVELDIR:", directory)
            try:
                if os.path.isfile(directory):
                    file_paths.append(directory)
                elif os.path.isdir(directory):
                    file_paths.extend(
                        [os.path.join(directory, file) for file in os.listdir(directory) if
                         os.path.isfile(os.path.join(directory, file))])
                else:
                    colorPrinter.print_warning(global_arg_dict, directory,
                                "is not a valid file or directory and will be "
                                "ignored!")
            except PermissionError:
                colorPrinter.print_warning(global_arg_dict, directory,
                            "is not accessible and will be ignored")

    def recursive_collect(dir):
        """
        recursively collects filenames from directories and all their
        subdirectories

        :dir: parent directory
        :returns: list of absolute filenames

        """
        temp_file_path_list = []
        try:
            for file in os.listdir(dir):
                if os.path.isfile(os.path.join(dir, file)):
                    temp_file_path_list.append(os.path.join(dir, file))
                elif os.path.isdir(os.path.join(dir, file)):
                    temp_file_path_list.extend(
                        recursive_collect(os.path.join(dir, file)))
        except PermissionError:
            colorPrinter.print_warning(global_arg_dict, dir,
                        "is not accessible and will be ignored!")
        return temp_file_path_list

    # collect all files from multi_level_dirs
    if multi_level_dirs:
        for directory in multi_level_dirs:
            colorPrinter.print_verbose(global_arg_dict,
                                       "NEXT MULTILEVELDIR:", directory)
            try:
                if os.path.isfile(directory):
                    file_paths.append(directory)
                if os.path.isdir(directory):
                    file_paths.extend(recursive_collect(directory))
                else:
                    colorPrinter.print_warning(global_arg_dict, directory,
                                "is not a valid file or directory and will be \
ignored!")
            except PermissionError:
                colorPrinter.print_warning(global_arg_dict, directory,
                            "is not accessible and will be ignored")

                # remove duplicates and sort
    file_paths = sorted(list(set(file_paths)))
    colorPrinter.print_verbose(global_arg_dict, "ALL FILES FROM SINGLE+MULTILEVELDIRS:",
                             file_paths)

    # if '-t' is specified, only test files with given endings
    if tested_file_types:
        # backwards to not saw off the branch we're sitting on
        for i in range(len(file_paths) - 1, -1, -1):
            match = 0
            for tFT in tested_file_types:
                if tFT[0] == '.':
                    tFT = '\\' + tFT        # to escape dots
                if re.search(tFT + '$', file_paths[
                    i]):    # true if filePath ends with fileTypePick
                    match = 1
            if match == 0:
                del file_paths[i]

    # if '-i' is specified, ignore files with given endings
    if ignored_file_types:
        if tested_file_types:
            ignored_file_types = list(
                # remove all ignored_file_types that are also tested_file_types
                set(ignored_file_types).difference(set(tested_file_types)))
        for i in range(len(file_paths) - 1, -1, -1):
            # needed if more than one ban to prevent index errors
            ban_file_path = False
            for iFT in ignored_file_types:
                if iFT[0] == '.':
                    iFT = '\\' + iFT  # to escape dots
                if re.search(iFT + '$', file_paths[i]):
                    ban_file_path = True
            if ban_file_path:
                del file_paths[i]

    #return list with absolute file paths
    colorPrinter.print_verbose(global_arg_dict, "TARGETS FROM SINGLE+MULTILEVELDIRS",
                               str(file_paths))
    return sorted(file_paths)