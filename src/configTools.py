"""
Copyright (C) 2014 Fabian Neuschmidt. All Rights Reserved.
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Fabian Neuschmidt (fabian.neuschmidt@googlemail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import configparser
import collections
from src import colorPrinter


def create_config(path_to_config, arg_dict):
    """ renames configuration file if it exists and creates new configuration
    file with default values.

    :pathToConfig: Location of configuration File
    :returns: None

    """

    colorPrinter.print_warning(arg_dict, '{} seems to be missing or corrupted. '
                                         'A new config file will be generated.'.format(os.path.abspath(path_to_config)))
    try:
        os.rename(path_to_config, path_to_config + '.old')
        print('{} backed up as {}'.format(path_to_config, path_to_config
                                          + '.old'))
    except FileNotFoundError:
        pass
    cfgparser = configparser.ConfigParser()
    cfgparser.optionxform = str        # this makes options case sensitive
    cfgparser['TARGETS'] = collections.OrderedDict([
        ('DefaultTargetLocation', ''),
        ('DefaultRecursiveTargetLocation', ''),
        ('DefaultCheckedFileTypes', ''),
        ('DefaultIgnoredFileTypes', '')
    ])
    cfgparser['FILTERS'] = collections.OrderedDict([
        ('DefaultFilters', ''),
        ('DefaultRegularFilterMatch', ''),
        ('FilterDir', './filters'),
    ])
    cfgparser['COLORS'] = collections.OrderedDict([
        ('FileFineColor', 'bright green'),
        ('FilePoorColor', 'bright red'),
        ('TextColor', 'normal'),
        ('FilterColor', 'dark gray'),
        ('VerboseInformationColor', 'cyan'),
        ('WarningColor', 'bright yellow')
    ])
    cfgparser['FILTERSETTINGS'] = collections.OrderedDict([
        ('UseTabs', 'false'),
        ('TabSize', '4'),
        ('MaxCharsPerLine', 80)
    ])
    with open(path_to_config, 'w') as configfile:
        cfgparser.write(configfile)
        print('created default configuration at {}'.format(path_to_config))


# NOTE if you change this function be sure to change it too in filters!
def get_setting(setting_identifier, arg_dict, default):
    """
    Returns the value of the setting.

    :setting_identifier: a string that you find in the settings file
    :arg_dict: the settings dictionary
    :default: the default value
    """
    if arg_dict.get(setting_identifier, None) is None:
        colorPrinter.print_warning(arg_dict, "{} is undefined. Setting default value ({}).".format(
            setting_identifier, default))
    val = arg_dict.get(setting_identifier, default)

    if str(val).lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup',
                            'certainly', 'uh-huh', 'j', 'ja']:
        val = True
    else:
        if str(val).lower() in ['false', '0', 'f', 'n', 'no', 'nope',
                                'never']:
            val = False
        else:
            if str(val).isnumeric():
                val = int(val)
    return val


def parse_config(path_to_config, arg_dict):
    """ parses arguments from configuration file

    :returns: parsed arguments in dictionary structure

    """
    cfgparser = configparser.ConfigParser()
    cfgparser.optionxform = str        # this makes options case sensitive

    if not os.path.exists(path_to_config):
        create_config(path_to_config, arg_dict)

    cfgparser.read(path_to_config)
    conf_vars = dict()
    conf_vars['d'] = cfgparser['TARGETS']['DefaultTargetLocation'].split(
        ',')
    conf_vars['dd'] = cfgparser['TARGETS'][
        'DefaultRecursiveTargetLocation'].split(',')
    conf_vars['f'] = cfgparser['FILTERS']['DefaultFilters'].split(',')
    conf_vars['ff'] = cfgparser['FILTERS'][
        'DefaultRegularFilterMatch'].split(',')
    conf_vars['t'] = cfgparser['TARGETS']['DefaultCheckedFileTypes'].split(
        ',')
    conf_vars['i'] = cfgparser['TARGETS']['DefaultIgnoredFileTypes'].split(',')

    # get all values from COLORS and FILTERS
    conf_vars.update(dict(cfgparser.items('COLORS')))
    conf_vars.update(dict(cfgparser.items('FILTERSETTINGS')))

    return conf_vars

