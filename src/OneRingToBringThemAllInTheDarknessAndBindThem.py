__author__ = 'fabian, lasse'


import os
import sys
import pkgutil
import inspect
import re
#
from src import colorPrinter


def collect_filter_functions(arg_dict):
    """
    imports filter functions from ../filters/ and returns them in a dictionary

    :returns: Dictionary as follows: {NameOfFunction:Function}

    """

    filter_names = arg_dict['f']
    regular_filter_matches = arg_dict['ff']

    filter_dict = {}

    #find /codeChecker/filters/ folder independent from current directory
    # isn't that what we do NOT want?
    filters_directory = os.path.realpath(os.path.abspath(
        os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0],
                     "../filters")))
    colorPrinter.print_verbose(arg_dict, "FILTERSDIRECTORY:", filters_directory)
    if filters_directory not in sys.path:
        sys.path.insert(0, filters_directory)

    # collect Rules according to filter_names and regular_filter_matches:
    for importer, packageName, _ in pkgutil.iter_modules(
            [filters_directory]): # all modules in filters/
        use_this_filter = False
        if packageName in filter_names:
            use_this_filter = True
        for regex in regular_filter_matches:
            if re.search(regex, packageName):
                use_this_filter = True
        if use_this_filter:
            module = importer.find_module(packageName).load_module(packageName)
            for functionName, function in inspect.getmembers(module):
                if functionName == 'cc_filter':
                    filter_dict[packageName] = function
                    colorPrinter.print_verbose(arg_dict, "ADDED FILTER:", packageName)
    return filter_dict