__author__ = 'fabian'

import os
import inspect
import pkgutil
import argparse

filter_choice_list = [
    package_name for importer, package_name, _ in
    pkgutil.iter_modules([os.path.realpath(
        os.path.abspath(os.path.join(
            os.path.split(inspect.getfile(inspect.currentframe()))[0],
            "filters")))])]


def parse_cli_args():
    """
    Parses command line arguments and configures help output.

    :returns: parsed arguments in dictionary structure
    """

    argparser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__,
        epilog='''\
    \rPossible choices for filters (-f):
    \r{}
    \r(Default is all)
    '''.format(filter_choice_list))

    argparser.add_argument('-d', nargs='+', metavar='DIR',
                           help="Directories or files to be checked")
    argparser.add_argument('-dd', nargs='+', metavar='DIR',
                           help="Directories to be checked including \
sub-directories")
    argparser.add_argument('-f', nargs='+', metavar='FILTER',
                           choices=filter_choice_list,
                           help="Filters to be applied")
    argparser.add_argument('-ff', nargs='+', metavar='REGEX',
                           help="regular expressions matching filters to apply")
    argparser.add_argument('-t', nargs='+', metavar='FILETYPE',
                           help="filetypes to be checked")
    argparser.add_argument('-i', nargs='+', metavar='FILETYPE',
                           help="filetypes to be ignored")
    argparser.add_argument('-c', nargs='?', metavar='FILE',
                           default="codeChecker.conf",
                           help="Configuration file")
    argparser.add_argument('-v', action='store_true', help="enable verbosity")
    arg_vars = vars(argparser.parse_args())
    return arg_vars
