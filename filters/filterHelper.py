#!/usr/bin/env python3

"""
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Lasse Schuirmann (lasse.schuirmann@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Lasse Schuirmann'


class ErrorMessage:
    def __init__(self, arg_dict, line, message):
        self.m_line = line
        self.m_arg_dict = arg_dict
        self.m_msg = message

    def print(self):
        print(color_code(self.m_arg_dict["FilterColor"]) + 'Line ' +
              str(self.m_line) + ': ' +
              color_code(self.m_arg_dict["TextColor"]) + self.m_msg +
              color_code('normal'))


class FilterResult:
    def __init__(self, arg_dict, name):
        self.m_arg_dict = arg_dict
        self.m_name = name
        self.m_err_list = []

    def append_error(self, err):
        if isinstance(err, ErrorMessage):
            self.m_err_list.append(err)
        else:
            print_warning(self.m_arg_dict, "Wrong invocation of FilterResult::"
                                           "append_error.")

    def append_error(self, line, message):
        self.m_err_list.append(ErrorMessage(self.m_arg_dict, line, message))


color_code_dict = {
    'black': '0;30',
    'bright gray': '0;37',
    'blue': '0;34',
    'white': '1;37',
    'green': '0;32',
    'bright blue': '1;34',
    'cyan': '0;36',
    'bright green': '1;32',
    'red': '0;31',
    'bright cyan': '1;36',
    'purple': '0;35',
    'bright red': '1;31',
    'yellow': '0;33',
    'bright purple': '1;35',
    'dark gray': '1;30',
    'bright yellow': '1;33',
    'normal': '0',
}


def color_code(color):
    return '\033[' + color_code_dict[color] + 'm'


def print_color(color, *args, end="\n"):
    """ prints arguments in specified color

    :color: Color in which the following arguments should be printed
    :*args: Any Number of Arguments, preferably strings or numbers
    :returns: None

    """

    try:
        print('\033[' + color_code_dict[color] + 'm', end='')
        for arg in args:
            print(arg, end=' ')
        print('\033[0m', end=end)
    except:
        for arg in args:
            print(arg, end=' ')
        print("", end=end)


def print_warning(arg_dict, *args, end="\n"):
    """ prints a warning

    :arg_dict: the settings dictionary for color extraction
    :*args: any number of printable arguments
    :returns: None
    """
    color = arg_dict.get('WarningColor', 'yellow')
    print_color(color, 'WARNING:', *args, end=end)


def print_verbose(arg_dict, *args, end="\n"):
    """ prints a verbose message if 'v' is true in the arg_dict

    :arg_dict: the settings dictionary for color extraction
    :*args: any number of printable arguments
    :returns: None
    """
    if arg_dict['v']:
        print_color(arg_dict['VerboseInformationColor'], 'VERBOSE:', *args, end=end)


def get_setting(setting_identifier, arg_dict, default):
    """
    Returns the value of the setting.

    :setting_identifier: a string that you find in the settings file
    :arg_dict: the settings dictionary
    :default: the default value
    """
    if arg_dict.get(setting_identifier, None) is None:
        print_warning(arg_dict, "{} is undefined. Setting default value ({}).".format(
            setting_identifier, default))
    val = arg_dict.get(setting_identifier, default)

    if str(val).lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup',
                            'certainly', 'uh-huh', 'j', 'ja']:
        val = True
    else:
        if str(val).lower() in ['false', '0', 'f', 'n', 'no', 'nope',
                                'never']:
            val = False
        else:
            if str(val).isnumeric():
                val = int(val)
    return val


def append_error(error_list, arg_dict, line_number, message):
    tmp = color_code(arg_dict["FilterColor"]) + 'Line ' + str(line_number) + ': ' + \
        color_code(arg_dict["TextColor"]) + message + color_code('normal')
    error_list.append(tmp)
