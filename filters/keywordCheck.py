#!/usr/bin/env python3

"""
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Lasse Schuirmann (lasse.schuirmann@gmail.com)
Copyright (C) 2013 Fabian Neuschmidt. All Rights Reserved.
Based on work by Fabian Neuschmidt (fabian.neuschmidt@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Lasse Schuirmann'

import re
import filterHelper

keywords = ["FIXME", "TODO", "BUG"]


def cc_filter(queue, line_list, arg_dict):
    """
    returns error if a keyword is found in the file

    :line_list: file as list of strings
    :returns: all error messages in a list
    """
    error_messages = ["Keyword check"]

    pattern = []
    for keyword in keywords:
        pattern.append(re.compile(r"\b{}\b".format(keyword)))

    for line_number, line in enumerate(line_list):
        for pattern_number, p in enumerate(pattern):
            if p.search(line):
                filterHelper.append_error(error_messages, arg_dict,
                                          line_number + 1,
                                          'Keyword found ("{}")'.format(keywords[pattern_number]))

    queue.put(error_messages)

