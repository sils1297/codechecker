#!/usr/bin/env python3

"""
Copyright (C) 2014 Lasse Schuirmann. All Rights Reserved.
Written by Lasse Schuirmann (lasse.schuirmann@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Lasse Schuirmann'

import filterHelper as cc_api


def cc_filter(queue, line_list, arg_dict):
    """
    returns error if a line is longer than 80 characters

    :arg_dict: contains all settings defined by the user
    :line_list: file as list of strings
    :returns: all error messages in a list
    """

    tabsize = cc_api.get_setting('TabSize', arg_dict, 4)
    max_chars = cc_api.get_setting('MaxCharsPerLine', arg_dict, 80)
    tab_usage = cc_api.get_setting('UseTabs', arg_dict, False)


    error_messages = ["Line check"]

    for line_number, line in enumerate(line_list):
        i = 0
        last_spaces = 0
        tab_fault = False
        space_fault = False
        spaces_before_tab = False
        for char in line:
            if char == '\t':
                i += tabsize
                tab_fault = (not tab_usage)
                # no spaces
                spaces_before_tab = last_spaces != 0
            else:
                i += 1
                if char == ' ':
                    last_spaces += 1
                    if last_spaces >= tabsize:
                        # if tabs are to be used this
                        space_fault = tab_usage
                else:
                    last_spaces = 0

        if i > max_chars:
            cc_api.append_error(error_messages, arg_dict, line_number + 1,
                                'Line exceeds {} characters '
                                '(found {}).'.format(max_chars, i))
        if spaces_before_tab:
            cc_api.append_error(error_messages, arg_dict, line_number + 1,
                                'Found spaces before a tab.')
        if tab_fault:
            cc_api.append_error(error_messages, arg_dict, line_number + 1,
                                'Found tabs. (Use {} spaces instead.)'
                                .format(tabsize))
        if space_fault:
            cc_api.append_error(error_messages, arg_dict, line_number + 1,
                                'Found {} or more continuous spaces. (Use '
                                'tabs instead.)'
                                .format(tabsize))

    queue.put(error_messages)

